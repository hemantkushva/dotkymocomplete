function [peakBitMap_Thresholded,peakValuesMap_ThresholdedMap] =peakThresh(imgArr,peakBitMap,nlevels)
%Runs a multiOtsu on the all the values of local maximas of an image.
peakIndicesArr=find(peakBitMap);
peakValuesArr=imgArr(peakIndicesArr); %#ok<FNDSB>
threshLevelsArr=multithresh(peakValuesArr,nlevels);
peakThresholdedLinearIndices=peakIndicesArr(imgArr(peakIndicesArr)>=threshLevelsArr(1));
peakBitMap_Thresholded=zeros(size(peakBitMap));
peakBitMap_Thresholded(peakThresholdedLinearIndices)=1;

% peakValuesMap=zeros(size(imgArr));
% peakValuesMap(peakIndicesArr)=imgArr(peakIndicesArr);
peakValuesMap_ThresholdedMap=zeros(size(imgArr));
peakValuesMap_ThresholdedMap(peakThresholdedLinearIndices)=imgArr(peakThresholdedLinearIndices);


end