function outputStruct=kymoProcessing(inputStruct,gaussianSTD)
filteringTemplate=gaussmf(-6:1:6,[gaussianSTD,0]);
nKymos=length(inputStruct);
for i=1:nKymos
outputStruct(i).kymo_dynamicMeanSubtraction=convKymo(inputStruct(i).kymo_dynamicMeanSubtraction,filteringTemplate);
% outputStruct(i).kymo_staticMeanSubtraction=convKymo(inputStruct(i).kymo_staticMeanSubtraction,filteringTemplate);
outputStruct(i).kymo_noSubtraction=mat2gray(convKymo(inputStruct(i).kymo_noSubtraction,filteringTemplate));
end
end


function convolvedKymo=convKymo(kymo,filteringTemplate)
kymoRowCell=arrayfun(@(k) conv(kymo(k,:),filteringTemplate,'same'),1:size(kymo,1),'uniformOutput',false);
convolvedKymo=vertcat(kymoRowCell{:});
end