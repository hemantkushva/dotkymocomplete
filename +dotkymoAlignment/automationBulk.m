function alignedKymoStruct=automationBulk(rawStruct,processedStruct)
import dotkymoAlignment.*

for i=1:length(rawStruct)
if(i==1)
    alignedKymoStruct=automation(processedStruct(i),rawStruct(i));
else
    alignedKymoStruct=[alignedKymoStruct,automation(processedStruct(i),rawStruct(i))];
end
end

end