function [barcode,signalImgArr,dilatedBitMask]=getMaskedBarcode(imgArr,bitMask,dilationRadius)
structuringElement=strel('line',(dilationRadius*2)+1,0);
dilatedBitMask=imdilate(bitMask,structuringElement);
signalImgArr=imgArr.*dilatedBitMask;
nnzPerColumns=arrayfun(@(k) nnz(dilatedBitMask(:,k)),1:size(imgArr,2));
normalizingConstantArr=nnzPerColumns;
normalizingConstantArr(normalizingConstantArr==0)=1;
barcode=sum(signalImgArr)./normalizingConstantArr;
end