function structA=automation(inputStruct_smooth,inputStruct_raw)
import dotkymoAlignment.*
settings=settings_dotKymoAlignment; % A struct containing all parameters used.


%After the background subtraction, a kymogrpah may have negative values which 
%can't be accurately displayed so a corresponding kymograph without any
%background subtraction
%is used for all visual demonstration purposes. All such results are marked
%with the suffix "_show"

raw=inputStruct_raw.kymo_dynamicMeanSubtraction;
raw_show=inputStruct_raw.kymo_noSubtraction;

imgArr=inputStruct_smooth.kymo_dynamicMeanSubtraction;
imgArr_show=inputStruct_smooth.kymo_noSubtraction;

peakBitMap=generatePeakBitMap(imgArr);
peakBitMap=peakBitMap.*(~(imgArr<=0));


%Remove all isolated pixels in the peakBitMap i.e pixles which dont have pixel above or below within the range specified by 'k'.
sel=zeros(3,2*settings.localFluctuationWindow+1);
sel(1,:)=1;
sel(3,:)=1;
tmp_1=imdilate(peakBitMap,sel); 
peakBitMap=peakBitMap.*tmp_1;



%Graph Based Computatations.
S=peakGraph3(imgArr,settings.localFluctuationWindow,peakBitMap);%Create a graph
featuresCellArray=peakGraphProcessing(S,settings.minimumSizeOfConnectedComponent);%Process the connected components and save only those whose size is greater than the threshold.
featuresCellArray_connected=connectGroups_2(featuresCellArray,size(imgArr),settings.featureMeanFluctuationWindow); % connect features corresponding to the same labelled segment.
markedIndividualFeatures=peekcolor(imgArr_show,featuresCellArray,0);%Mark obtained  individual features on the kymograph for visual demonstration.
markedConnectedFeatures=peekcolor(imgArr_show,featuresCellArray_connected,0);%Mark connected features on the kymograph for visual demonstration.
[smoothAligned,rawAligned,featureBitMap]=alignImage_final(featuresCellArray_connected,imgArr,raw);%Align the original kymograph with background(bg) subtracted.
[smoothAligned_show,rawAligned_show,~]=alignImage_final(featuresCellArray_connected,imgArr_show,raw_show);%Align the kymograph with no bg subtraction for visual demonstration. 
[barcode,signalImgArr,dilatedBitMask]=getMaskedBarcode(rawAligned,featureBitMap,5);%Get the signalKymograph and the barcode for the original kymograph.
[~,signalImgArr_show,~]=getMaskedBarcode(rawAligned_show,featureBitMap,5);%Get the signalKymograph and the barcode for the kymograph without bg subtraction for visual demonstration.



%Delete all the variables which are not desired in the output.
clear sel tmp_1 inputStruct_smooth inputStruct_raw basis imgArr imgArr_show raw raw_show featureBitMap settings
%Save the resulting variables in a struct.
structA=v2struct();

 
end
