function outputStruct= newMethod(imgArr,stdMultiple,type)
peakBitMap=dotkymoAlignment.generatePeakBitMap(imgArr);

sel=strel('line',11,0);
dilatedPeakBitMap=imdilate(peakBitMap,sel);
nIterations=2;



if(strcmp(type,'global'))
    backGroundValuesArr=imgArr(~(logical(dilatedPeakBitMap)));
    
    peakValuesArr=nonzeros(logical(peakBitMap));
    peakValuesArr=peakValuesArr(peakValuesArr>0);
    
    iterations=1:nIterations;
    for i=iterations
        nElements(i)=length(peakValuesArr);
        nElementsBackground(i)=length(backGroundValuesArr);
        meanBackGroundArr(i)=mean(backGroundValuesArr);
        stdBackGroundArr(i)=std(backGroundValuesArr);
        thresholdArr(i)=meanBackGroundArr(i)+stdMultiple*stdBackGroundArr(i);
        nFilteredOut(i)=length(find((peakValuesArr<thresholdArr(i))));
        peakValuesArr=peakValuesArr(peakValuesArr>=thresholdArr(i));
        backGroundValuesArr=[backGroundValuesArr,peakValuesArr(peakValuesArr<thresholdArr(i))];
    end
    figure('Name',sprintf('%dTimesSTD',stdMultiple)),plot(iterations,meanBackGroundArr,'--*','DisplayName','Mean');hold on;
    plot(iterations,stdBackGroundArr,'--^','DisplayName','STD');hold on;
    plot(iterations,thresholdArr,'--v','DisplayName','Threshold');hold on;
    legend('show','Location','NorthWest')
    ax=gca;
    ax.Title.String=sprintf('Threshold: Mean+%d*STD',stdMultiple);
    ax.XLim=[0 iterations(end)];
    correctWhiteSpace();
    for i=1:iterations(end)
        if(nElements(i)~=0)
            text(iterations(i),thresholdArr(i)+0.02,sprintf('%d',nElements(i)),'FontSize',15)
        end
    end
    
else if(strcmp(type,'local'))
       for i=1:nIterations
            if(i==1)
                for j=1:1000
                    tmp=imgArr(j,:);
                    values_signal=tmp(logical(dilatedPeakBitMap(j,:)));
                    values_signal=values_signal(values_signal>0);
                    peakValuesArr{i}{j}=values_signal;
                    values_bg=tmp(~logical(dilatedPeakBitMap(j,:)));
                    bgValuesArr{i}{j}=values_bg;
                    clear values_signal values_bg
                end
            else
                peakValuesArr{i}=arrayfun(@(k) peakValuesArr{i-1}{k}(peakValuesArr{i-1}{k}>=thresholdArr{i-1}(k)),1:1000,'uniformoutput',false);
                bgValuesArr{i}=arrayfun(@(k) [bgValuesArr{i-1}{k},peakValuesArr{i-1}{k}(peakValuesArr{i-1}{k}<thresholdArr{i-1}(k))],1:1000,'uniformoutput',false);
                
            end
            nSignal{i}=arrayfun(@(k) length(peakValuesArr{i}{k}),1:1000);
            nbg{i}=arrayfun(@(k) length(bgValuesArr{i}{k}),1:1000);
            meanbgArr{i}=arrayfun(@(k) mean(bgValuesArr{i}{k}), 1:1000);
            stdbgArr{i}=arrayfun(@(k) std(bgValuesArr{i}{k}), 1:1000);
            meanSignalArr{i}=arrayfun(@(k) mean(peakValuesArr{i}{k}),1:1000);
            stdSignalArr{i}=arrayfun(@(k) std(peakValuesArr{i}{k}),1:1000);
            thresholdArr{i}=arrayfun(@(k) meanbgArr{i}(k)+(stdMultiple*stdbgArr{i}(k)),1:1000);
       end
       colorsArr = distinguishable_colors(nIterations);
       nbreaks=0;
   f1=figure('Name',sprintf('fig_5_BackGround Mean,%d Breaks',nbreaks));
   f2=figure('Name',sprintf('fig_5_BackGround STD,%d Breaks',nbreaks));
    f3=figure('Name',sprintf('fig_5_Signal Mean,%d Breaks',nbreaks));
   f4=figure('Name',sprintf('fig_5_Signal STD,%d Breaks',nbreaks));
   for i=2
   figure(f1);plot(1:1000,meanbgArr{i},'-','Color',colorsArr(1,:),'DisplayName','Actual Data'),hold on;   
   figure(f2);plot(1:1000,stdbgArr{i},'-','Color',colorsArr(1,:),'DisplayName','Actual Data'),hold on;
   figure(f3);plot(1:1000,meanSignalArr{i},'-','Color',colorsArr(1,:),'DisplayName','Actual Data');hold on;
   figure(f4);plot(1:1000,stdSignalArr{i},'-','Color',colorsArr(1,:),'DisplayName','Actual Data');hold on;
   end
%    fit1=splinefit(1:1000,meanbgArr{i},50);
%    fit2=splinefit(1:1000,stdbgArr{i},50);
%    fit3=splinefit(1:1000,meanSignalArr{i},50);
%    fit4=splinefit(1:1000,stdSignalArr{i},50);
%    figure(f1),plot(1:1000,ppval(1:1000,fit1),'Color',colorsArr(2,:),'DisplayName','Obtained Fit'),legend('show')
%    figure(f2),plot(1:1000,ppval(1:1000,fit2),'Color',colorsArr(2,:),'DisplayName','Obtained Fit'),legend('show')
%    figure(f3),plot(1:1000,ppval(1:1000,fit3),'Color',colorsArr(2,:),'DisplayName','Obtained Fit'),legend('show')
%    figure(f4),plot(1:1000,ppval(1:1000,fit4),'Color',colorsArr(2,:),'DisplayName','Obtained Fit'),legend('show')
%    
   
   
   
   
   
   g = fittype( @(b,x) b*exp(-x) );
   
   
   
   
   
%    fit1=fit((1:1000)',(meanbgArr{i})',g);
%    fit2=fit((1:1000)',(stdbgArr{i})',g);
   fit3=fit((1:1000)',(meanSignalArr{i})',g);
   fit4=fit((1:1000)',(stdSignalArr{i})',g);
%    figure(f1),plot(1:1000,fit1(1:1000),'Color',colorsArr(2,:),'DisplayName','Obtained Fit'),legend('show')
%    figure(f2),plot(1:1000,fit2(1:1000),'Color',colorsArr(2,:),'DisplayName','Obtained Fit'),legend('show')
   figure(f3),plot(1:1000,fit3(1:1000),'Color',colorsArr(2,:),'DisplayName','Obtained Fit'),legend('show')
   figure(f4),plot(1:1000,fit4(1:1000),'Color',colorsArr(2,:),'DisplayName','Obtained Fit'),legend('show')
   
   
   
   
   
   
   
   
   
   
   
   
   
   figure(f1);ax=gca;ax.YLabel.String='Intensity';ax.XLabel.String='Frame #';correctWhiteSpace();
   figure(f2);ax=gca;ax.YLabel.String='Intensity';ax.XLabel.String='Frame #';correctWhiteSpace();
   figure(f3);ax=gca;ax.YLabel.String='Intensity';ax.XLabel.String='Frame #';correctWhiteSpace();
   figure(f4);ax=gca;ax.YLabel.String='Intensity';ax.XLabel.String='Frame #';correctWhiteSpace();
   
   
   
   
%    printFigures();    
       

    end
    
% for i=1:1000
%     tmp_1=peakBitMap(i,:);
%     tmp_2=imgArr(i,:);
%     modifiedMap(i,:)=(tmp_2.*tmp_1>thresholdArr{1}(i)) & logical(tmp_1);
% end
    
    outputStruct=v2struct(meanbgArr,stdbgArr,meanSignalArr,stdSignalArr);
    
    
end