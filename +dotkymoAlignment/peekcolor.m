
function colorImg= peekcolor(imgArr,featuresCellArray,typeNumber)

imgSizeArr=size(imgArr);

if(typeNumber==1)
colorImg=ones([imgSizeArr,3]);
else
    colorImg=repmat(imgArr,1,1,3);
end

[rowsCell,colsCell]=cellfun(@(linearIndxArray) arrayfun(@(linearIndx)ind2sub(imgSizeArr,linearIndx),linearIndxArray),featuresCellArray,'uniformoutput',false);
RGBcombinations=distinguishable_colors(length(featuresCellArray));
for i=1:length(rowsCell)
    rowArr=rowsCell{i};
    colArr=colsCell{i};
    for j=1:length(rowArr)
        colorImg(rowArr(j),colArr(j),1:3)=RGBcombinations(i,:);
    end
    clear rowArr colArr
end





end
