function [alignedImage_smooth,alignedImage_raw,featureBitMap]=alignImage_final(featuresCellArray_connected,imgArr_smooth,raw)

%{
This function does a synchronous multiple feature alignment of a kymograph.

INPUTS:

"featuresCellArray_connected"-a cell array such that each cell contains all
the linear index of all the pixels belongin to that feature.

"imgArr_smooth"- the smooth version of kymograph.

"raw" - the raw kymograph.

OUTPUTS:

"alignedImage_smooth"- aligned version of smooth kymograph.

"alignedImage_raw"- aligned version of the raw kymograph.

"featureBitMap" -a binary image with foreground pixels on the most likely
position of a gaussian peak after alignment.


%}


alignedImage_smooth=imgArr_smooth;
alignedImage_raw=raw;
sizeArr=size(imgArr_smooth);
rows=sizeArr(1);
cols=sizeArr(2);
nFeatures=length(featuresCellArray_connected);

[rowCellArr,colCellArr]=cellfun(@(nodesArr) ind2sub(sizeArr,nodesArr),featuresCellArray_connected,'uniformOutput',false);
featureMeanArr=cellfun(@(colArr)  mean(colArr),colCellArr);

featureBitMap=zeros(sizeArr);
for i=1:nFeatures
featureCol=round(featureMeanArr(i));
featureLikelyNodeNumber=arrayfun(@(k) sub2ind(sizeArr,rowCellArr{i}(k),featureCol),1:length(rowCellArr{i}));
featureBitMap(featureLikelyNodeNumber)=1;
end


 
nodeMatrix=zeros(sizeArr);
meanMatrix=zeros(sizeArr);
for i=1:nFeatures
nodeMatrix(featuresCellArray_connected{i})=featuresCellArray_connected{i};
meanMatrix(featuresCellArray_connected{i})=featureMeanArr(i);
end
sparse_nodeMatrix=sparse(nodeMatrix);
sparse_meanMatrix=sparse(meanMatrix);

pixelStretchValue=ones(sizeArr);

for i=1:rows
 featureColArr=find(sparse_nodeMatrix(i,:));
 meanArr=nonzeros(sparse_meanMatrix(i,:))';
 if(~isempty(featureColArr))
 featureColArr_relative=[featureColArr(1),diff(featureColArr),cols-featureColArr(end)];
 meanArr_relative=[meanArr(1),diff(meanArr),cols-meanArr(end)];  
 
 stretchValue=meanArr_relative./featureColArr_relative;
 
 pixelStretchValue(i,1:featureColArr(1))=stretchValue(1);
 
 for j=2:length(featureColArr)
    pixelStretchValue(i,featureColArr(j-1)+1:featureColArr(j)) =stretchValue(j);   
 end
 
 pixelStretchValue(i,featureColArr(end)+1:end)=stretchValue(end);
 cmpXVals = cumsum(pixelStretchValue(i, :));
 cmpIntensityVals_smooth = interp1(cmpXVals, imgArr_smooth(i,:), 1:cols, 'spline');
 cmpIntensityVals_raw = interp1(cmpXVals, raw(i,:), 1:cols, 'spline');
 alignedImage_smooth(i,:)=cmpIntensityVals_smooth;
 alignedImage_raw(i,:)=cmpIntensityVals_raw;
 else
     continue;
 end
 
 end
 
end