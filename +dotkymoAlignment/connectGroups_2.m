function [featuresCellArray_connected]= connectGroups_2(featuresCellArray,sizeArr,featureMeanFluctuation,S)

%In this one I will use shortest path in case of any collision.



nFeatures=length(featuresCellArray);
%get the rows and columns of the top and last nodes of each feature.
[topNodeArr,bottomNodeArr]=arrayfun(@(k) deal(featuresCellArray{k}(1),featuresCellArray{k}(end)),1:nFeatures);
[topNode_rowArr,topNode_colArr]= arrayfun(@(k) ind2sub(sizeArr,topNodeArr(k)),1:nFeatures);
[bottomNode_rowArr,bottomNode_colArr]= arrayfun(@(k) ind2sub(sizeArr,bottomNodeArr(k)),1:nFeatures);


[~,colCellArr]=cellfun(@(nodesArr) ind2sub(sizeArr,nodesArr),featuresCellArray,'uniformOutput',false);
featureMeanArr=cellfun(@(colArr)  mean(colArr),colCellArr);




featureGraph=graph();
featureGraph=addnode(featureGraph,nFeatures); %Adds nFeatures nodes to the graph, without any connection
for i=1:nFeatures
 rowBasedIndx=bottomNode_rowArr(i)<topNode_rowArr; %potential daughters
 
 meanBasedIndx=featureMeanArr(i)-featureMeanFluctuation<= featureMeanArr & featureMeanArr <= featureMeanArr(i)+featureMeanFluctuation;
 
%  colBasedIndx=bottomNode_colArr(i)-3<= topNode_colArr & topNode_colArr <= bottomNode_colArr(i)+3;
%  combinedIndx=find(rowBasedIndx & colBasedIndx);
  combinedIndx=find(rowBasedIndx & meanBasedIndx);
  featureGraph=addedge(featureGraph,i,combinedIndx);
end
%Connected components could still have overlapping rows
connectedFeatures_indx_cellArr=conncomp(featureGraph,'outputform','cell'); %find connected components.
indxToCheck=find(arrayfun(@(k) length(connectedFeatures_indx_cellArr{k}),1:length(connectedFeatures_indx_cellArr))>=3);
for i=indxToCheck
    member_topNodeRowArr=topNode_rowArr(connectedFeatures_indx_cellArr{i});
    member_bottomNodeRowArr=bottomNode_rowArr(connectedFeatures_indx_cellArr{i});

    [sorted_topNode,sortOrder]=sort(member_topNodeRowArr);
    sorted_bottomNode=member_bottomNodeRowArr(sortOrder);
    
    count=0;
    for j=1:length(connectedFeatures_indx_cellArr{i})
    tmp_1=sorted_bottomNode(j)>=sorted_topNode;
    tmp_1(1:j)=0;
    count=count+nnz(tmp_1);
    end
    if(count>0)
       nodeNumbersArr= connectedFeatures_indx_cellArr{i};
       nodePairsArr=nchoosek(nodeNumbersArr,2);
       edgeIndx=nonzeros(arrayfun(@(k) findedge(featureGraph,nodePairsArr(k,1),nodePairsArr(k,2)),1:size(nodePairsArr,1)));
      featureGraph=rmedge(featureGraph,edgeIndx);
    end
end

connectedFeatures_indx_cellArr_clashResolved=conncomp(featureGraph,'outputform','cell');

nFeatures_clashRemoved=numel(connectedFeatures_indx_cellArr_clashResolved);

featuresCellArray_connected=arrayfun(@(k) vertcat(featuresCellArray{connectedFeatures_indx_cellArr_clashResolved{k}}),1:nFeatures_clashRemoved,'UniformOutPut',false);


end