function amplifiedMovie=amplifyMovie(movie)
%{
movie must have it's otulier pixels removed.
%}
smoothMovie=imgaussfilt3(movie,[2,1,2]);
smoothMovie=mat2gray(smoothMovie);
%Gamma correction with gamma=2, make the darker region more dark
amplifiedMovie=mat2gray(smoothMovie.^2);
end