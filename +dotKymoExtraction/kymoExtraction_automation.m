function outputStruct= kymoExtraction_automation(movieAddressORmovieMatrix,setting)
if(ndims(movieAddressORmovieMatrix)==3)
   movieMatrix=mat2gray(movieAddressORmovieMatrix);
else
    movieMatrix=dataProcessing.getMatrixFromTiff(movieAddressORmovieMatrix,1);
end

nFrames=size(movieMatrix,3);
if(setting.clean==true)
cleanedMovie=dataProcessing.removeHotPixelsFromMovieMatrix(movieMatrix,1,0);
else
cleanedMovie=movieMatrix;
end
amplifiedMovie=dotKymoExtraction.amplifyMovie(cleanedMovie);
if(setting.rotate==true)
moleculeOrientation=dotKymoExtraction.findMoleculeOrientation(amplifiedMovie(:,:,1),0.1);
rotatedMovie=imrotate(cleanedMovie,-moleculeOrientation);
rotatedMovie(rotatedMovie==0)=nan;
rotatedAmplifiedFrame=imrotate(amplifiedMovie(:,:,1),-moleculeOrientation);
else
rotatedMovie=cleanedMovie;
rotatedAmplifiedFrame=amplifiedMovie(:,:,1);
end

%Find the row corresponding to the vertical centre of the 2-d gaussians
colSum=sum(rotatedAmplifiedFrame,2);
[~,centreRow]=max(colSum);
[r,~]=size(rotatedAmplifiedFrame);


backgroundMatrix=rotatedMovie([1:max(centreRow-6,1),min(centreRow+6,r):end],:,:);
totalbackgroundMean=nanmean(backgroundMatrix(~isnan(backgroundMatrix)));
totalbackgroundSTD=std(backgroundMatrix(~isnan(backgroundMatrix)));

individualFrameMean=arrayfun(@(k) nanmean(nanmean(backgroundMatrix(:,:,k))), 1:nFrames);
individualFrameMean(isnan(individualFrameMean))=0;
kymoCell_1=arrayfun(@(k) nanmean(rotatedMovie(max(centreRow-5,1):min(centreRow+5,r),:,k))-individualFrameMean(k),1:nFrames,'UniformOutput',false);
kymoCell_2=arrayfun(@(k) nanmean(rotatedMovie(max(centreRow-5,1):min(centreRow+5,r),:,k))-totalbackgroundMean,1:nFrames,'UniformOutput',false);
kymoCell_3=arrayfun(@(k) nanmean(rotatedMovie(max(centreRow-5,1):min(centreRow+5,r),:,k)),1:nFrames,'UniformOutput',false );

               
kymo_dynamicMeanSubtraction=vertcat(kymoCell_1{:});
kymo_staticMeanSubtraction=vertcat(kymoCell_2{:});
kymo_noSubtraction=mat2gray(vertcat(kymoCell_3{:}));

clear  amplifiedMovie colSum r kymoCell_1 kymoCell_2 kymoCell_3 backgroundMatrix centreRow rotatedAmplifiedFrame rotatedMovie cleanedMovie movieMatrix cleanedMovie movieAddressORmovieMatrix nFrames  
outputStruct=v2struct();
end