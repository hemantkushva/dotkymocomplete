function orientation= findMoleculeOrientation(amplifiedFrame,angle_leastCount)
thetaCandidates=-90:angle_leastCount:90;
projectionsArr=radon(amplifiedFrame,thetaCandidates);
[~,maxValIndx]=max(max(projectionsArr));
perpendicualarAngle=thetaCandidates(maxValIndx);
orientation=perpendicualarAngle-90;
end
