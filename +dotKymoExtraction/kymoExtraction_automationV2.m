function outputStruct= kymoExtraction_automationV2(movieAddressORmovieMatrix,setting)
if(ndims(movieAddressORmovieMatrix)==3)
   movieMatrix=mat2gray(movieAddressORmovieMatrix);
else
    movieMatrix=dataProcessing.getMatrixFromTiff(movieAddressORmovieMatrix,1);
end

nFrames=size(movieMatrix,3);

preprocessingInformation=setting;
if(setting.clean==true)
cleanedMovie=dataProcessing.removeHotPixelsFromMovieMatrix(movieMatrix,1,0);
else
cleanedMovie=movieMatrix;
end
amplifiedMovie=dotKymoExtraction.amplifyMovie(cleanedMovie);
if(setting.rotate==true)
moleculeOrientation=dotKymoExtraction.findMoleculeOrientation(amplifiedMovie(:,:,1),0.1);
rotatedMovie=imrotate(cleanedMovie,-moleculeOrientation);
rotatedMovie(rotatedMovie==0)=nan;
rotatedAmplifiedFrame=imrotate(amplifiedMovie(:,:,1),-moleculeOrientation);

preprocessingInformation.moleculeOrientation=moleculeOrientation;

else
rotatedMovie=cleanedMovie;
rotatedAmplifiedFrame=amplifiedMovie(:,:,1);
end

completeInformation=preprocessingInformation;


%Find the row corresponding to the vertical centre of the 2-d gaussians
colSum=sum(rotatedAmplifiedFrame,2);
[~,centreRow]=max(colSum);
[r,c]=size(rotatedAmplifiedFrame);

Y_ROI=setting.halfHeight_ROI;

if(centreRow-(Y_ROI+1)>=1 && centreRow+Y_ROI<=r)
    disp('Sufficient Foreground for Generating Kymograph-> Generating Kymograph...')
    
    %Getting background Info
    backgroundMatrix=rotatedMovie([1:max(centreRow-(Y_ROI+1),1),min(centreRow+(Y_ROI+1),r):end],:,:);
    totalbackgroundMean=nanmean(backgroundMatrix(~isnan(backgroundMatrix)));
    totalbackgroundSTD=std(backgroundMatrix(~isnan(backgroundMatrix)));
    individualFrameMean=arrayfun(@(k) nanmean(nanmean(backgroundMatrix(:,:,k))), 1:nFrames);
    individualFrameMean(isnan(individualFrameMean))=0;
    
    backGroundInformation=v2struct(totalbackgroundMean,totalbackgroundSTD,individualFrameMean);
    
    
    kymoCell_1=arrayfun(@(k) nanmean(rotatedMovie(max(centreRow-Y_ROI,1):min(centreRow+Y_ROI,r),:,k))-individualFrameMean(k),1:nFrames,'UniformOutput',false);
    kymoCell_2=arrayfun(@(k) nanmean(rotatedMovie(max(centreRow-Y_ROI,1):min(centreRow+Y_ROI,r),:,k))-totalbackgroundMean,1:nFrames,'UniformOutput',false);
    kymoCell_3=arrayfun(@(k) nanmean(rotatedMovie(max(centreRow-Y_ROI,1):min(centreRow+Y_ROI,r),:,k)),1:nFrames,'UniformOutput',false );
    
    
    kymo_dynamicMeanSubtraction=vertcat(kymoCell_1{:});
    kymo_staticMeanSubtraction=vertcat(kymoCell_2{:});
    kymo_noSubtraction=mat2gray(vertcat(kymoCell_3{:}));
    
    
    
    clear kymoCell_1 kymoCell_2 kymoCell_3 backgroundMatrix
    
    posFG=[2,centreRow-(Y_ROI),c-2,Y_ROI*2+1];
    markedImage=insertShape(rotatedAmplifiedFrame,'rectangle',posFG,'color','green');
    
    generatedKymoGraphs=v2struct( kymo_dynamicMeanSubtraction, kymo_staticMeanSubtraction,kymo_noSubtraction);
    
    completeInformation.backGroundInformation=backGroundInformation;
    completeInformation.foregroundKymographs=generatedKymoGraphs;
    completeInformation.markedImage=markedImage;
    
    
    if(centreRow-(Y_ROI+1)>1)
        disp('Sufficient Upper Background-> Generating Upper Background Kymograph..')
        
        kymoCell_backGroundAbove=arrayfun(@(k) nanmean(rotatedMovie(1:max(centreRow-(Y_ROI+1),1),:,k)),1:nFrames,'UniformOutput',false );
        completeInformation.kymo_backUpper=vertcat(kymoCell_backGroundAbove{:});
        clear kymoCell_backGroundAbove
        
        posBG_above=[2,1,c-2,centreRow-Y_ROI-1];
        completeInformation.markedImage=insertShape(completeInformation.markedImage,'rectangle',posBG_above,'color','red');
        disp('YOLO')
        
    
    else
        warning('Not Sufficient upper background->Not Generating Lower Background Kymograph')
    end
    
    if(centreRow+(Y_ROI+1)<r)
        disp('Sufficient Lower Background-> Generating Lower Background Kymograph..')
        
        kymoCell_backGroundBelow=arrayfun(@(k) nanmean(rotatedMovie(min(centreRow+(Y_ROI+1),r):end,:,k)),1:nFrames,'UniformOutput',false);
        completeInformation.kymo_BackBelow=vertcat(kymoCell_backGroundBelow{:});
        clear kymoCell_backGroundBelow
        
        posBG_below=[2,centreRow+Y_ROI+1,c-2,r-(centreRow+Y_ROI)];
        completeInformation.markedImage=insertShape(completeInformation.markedImage,'rectangle',posBG_below,'color','red');
        
    else
        warning('Not Sufficient Lower Background->Not Generating Background Kymograph')
       
    end
    
    
else
    
    


end

clear  amplifiedMovie colSum r  centreRow rotatedAmplifiedFrame rotatedMovie cleanedMovie movieMatrix cleanedMovie movieAddressORmovieMatrix nFrames  
outputStruct=completeInformation;
end