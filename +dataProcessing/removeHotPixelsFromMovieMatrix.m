function cleanMovie=removeHotPixelsFromMovieMatrix(movieMatrix,normalizeSwitchMovie,normalizeSwitchFrame)
import dataProcessing.*
nFrames=size(movieMatrix,3);
cleanFramesCellArr=arrayfun(@(k)  removeHotPixels_v2(movieMatrix(:,:,k),normalizeSwitchFrame), 1:nFrames,'uniformoutput',false);
cleanMovie=cat(3,cleanFramesCellArr{:});

if(normalizeSwitchMovie==1)
cleanMovie=mat2gray(cleanMovie);
end


end
