function [cleanImage]=removeHotPixels_v2(imgArr,normalizeSwitch)
%{
This function replaces all the hot pixels in an image by it's corresponding
median filtered value.

Hot Pixels are isolated pixels in an image with disproportionately high
value which reduces the overall contrast of the image.

To identify hot pixels in an image, we take it's corresponding median
filtered image and look at the absolute value of the difference between the
images.
As mentioned above, since a hot pixel is isolated and has significantly
high value, it's corresponding median value is very low thus giving a
sharp maxima  in the differece image at that point.

Technical Note: MATLAB function medfilt2 uses a 3*3 box for calcualting the
median value and for the boundaries it assumes a default padding of '0',
irrespective of the actual distribution of the values in the image.
Therefore to avoid any error, the image must be padded with the median(or mode)
value of the entire image.

It's recommended to run this function on a image with a significant
background size.

Author: Hemant Kumar

%}

modeValue=mode(imgArr(:));
imgArr_padded=padarray(imgArr,[1,1],modeValue);
imgArr_padded_medianFiltered=medfilt2(imgArr_padded);
imgArr_medianFiltered=imgArr_padded_medianFiltered(2:end-1,2:end-1);

imgArr_normalized=mat2gray(imgArr); 
modeValue_normalized=mode(imgArr_normalized(:));
imgArr_normalized_padded=padarray(imgArr_normalized,[1,1],modeValue_normalized);
imgArr_normalized_padded_medianFiltered=medfilt2(imgArr_normalized_padded);
imgArr_normalized_medianFiltered=imgArr_normalized_padded_medianFiltered(2:end-1,2:end-1);
ratioImage=(imgArr_normalized_medianFiltered./imgArr_normalized);

hotPixelMap=ratioImage>2 | ratioImage<0.5;

cleanImage=imgArr.*(~hotPixelMap)+imgArr_medianFiltered.*hotPixelMap;

if(normalizeSwitch==1)
    cleanImage=mat2gray(cleanImage);
end

end




















