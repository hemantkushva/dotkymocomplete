function movie=getMatrixFromTiff(fileAddress,normalizeSwitch)
%{
Get a matrix movie from a tif file.
 normalizeSwitch: '1'-normalize the movie
                  '0'-do not normalize
%}
info=imfinfo(fileAddress);
nRows=mean([info.Height]); % If all frames have the same dimension then no issues, otherwise the average size is taken.
nCols=mean([info.Width]);
nFrames=numel(info);
movie=zeros(nRows,nCols,nFrames);
for i=1:nFrames
   tmpFrame=imread(fileAddress,i);
   movie(:,:,i)=tmpFrame;
end

if(normalizeSwitch==1)
   movie=mat2gray(movie);
end


end
