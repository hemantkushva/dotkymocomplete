function outputStruct=removeHotPixels(imgArr,normalizeSwitch)
%{
This function replaces all the hot pixels in an image by it's corresponding
median filtered value.

Hot Pixels are isolated pixels in an image with disproportionately high
value which reduces the overall contrast of the image.

To identify hot pixels in an image, we take it's corresponding median
filtered image and look at the absolute value of the difference between the
images.
As mentioned above, since a hot pixel is isolated and has significantly
high value, it's corresponding median value is very low thus giving a
sharp maxima  in the differece image at that point.

Technical Note: MATLAB function medfilt2 uses a 3*3 box for calcualting the
median value and for the boundaries it assumes a default padding of '0',
irrespective of the actual distribution of the values in the image.
Therefore to avoid any error, the image must be padded with the median(or mode)
value of the entire image.

It's recommended to run this function on a image with a significant
background size.

Author: Hemant Kumar

%}

modeValue=mode(imgArr(:));
imgArr_padded=padarray(imgArr,[1,1],modeValue);
imgArr_padded_medianFiltered=medfilt2(imgArr_padded);
imgArr_medianFiltered=imgArr_padded_medianFiltered(2:end-1,2:end-1);

imgArr_normalized=mat2gray(imgArr); 
modeValue_normalized=mode(imgArr_normalized(:));
imgArr_normalized_padded=padarray(imgArr_normalized,[1,1],modeValue_normalized);
imgArr_normalized_padded_medianFiltered=medfilt2(imgArr_normalized_padded);
imgArr_normalized_medianFiltered=imgArr_normalized_padded_medianFiltered(2:end-1,2:end-1);
diffImage=abs(imgArr_normalized_medianFiltered-imgArr_normalized);


%Calculating threshold by simple std calculation
r=groot;
h=histogram(diffImage,'Visible','off');
diff_y=(h.Values)';
diff_x=(h.BinEdges)';
diff_x=diff_x(1:end-1);
tmp_repeatedInProportion=arrayfun(@(k) repmat(diff_x(k),diff_y(k),1),1:length(diff_x),'UniformOutput',false);
dataInProportion=vertcat(tmp_repeatedInProportion{:});
stdData=std(dataInProportion);
threshold=4*stdData;

hotPixelMap=(diffImage>threshold);

cleanImage=imgArr.*(~hotPixelMap)+imgArr_medianFiltered.*hotPixelMap;

if(normalizeSwitch==1)
    cleanImage=mat2gray(cleanImage);
end



outlierArr=diffImage(diffImage>threshold);
minOutlier=min(outlierArr);
maxOutlier=max(outlierArr);



outputStruct=v2struct(cleanImage,stdData,threshold,diffImage,minOutlier,maxOutlier,outlierArr);

end

