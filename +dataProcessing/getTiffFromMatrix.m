function getTiffFromMatrix(movieMatrix,fileNameWithTiffExtension)
nFrames=size(movieMatrix,3);
for i=1:nFrames
imwrite(movieMatrix(:,:,i),fileNameWithTiffExtension,'Writemode','append');    
end